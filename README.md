# ign-common2-release

The ign-common2-release repository has moved to: https://github.com/ignition-release/ign-common2-release

Until May 31st 2020, the mercurial repository can be found at: https://bitbucket.org/osrf-migrated/ign-common2-release
